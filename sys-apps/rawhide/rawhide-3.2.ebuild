# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Rawhide lets you search for files on the command line using expressions and user-defined functions in a mini-language inspired by C"
HOMEPAGE="https://raf.org/rawhide/"
SRC_URI="https://raf.org/rawhide/download/rawhide-3.2.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

IUSE="acl ea attr magic pcre2"

src_configure() {
	./configure \
		--prefix=/usr
		$(use_enable acl) \
		$(use_enable attr) \
		$(use_enable pcre2) \
		$(use_enable magic) \
		$(use_enable ea)
}
