# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

: ${CMAKE_MAKEFILE_GENERATOR:='emake'}

inherit git-r3 cmake

DESCRIPTION="An open-source, instant-replay solution for Linux."
HOMEPAGE="https://github.com/matanui159/ReplaySorcery"
#SRC_URI="https://github.com/matanui159/ReplaySorcery/archive/${PV}.tar.gz"

EGIT_REPO_URI="https://github.com/matanui159/ReplaySorcery"
EGIT_SUBMODULES=( '*' )

if [[ ${PV} == *9999 ]] ; then
	EGIT_BRANCH="master"
else
	EGIT_COMMIT="${PV}"
fi

CMAKE_IN_SOURCE_BUILD=1

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND="dev-lang/nasm"

#src_configure() {
#	cmake -B build -DCMAKE_BUILD_TYPE="Release"
#}

#src_compile() {
#	emake -C build
#}

#src_install() {
#	emake -C build install
#}
